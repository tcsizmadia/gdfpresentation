package edu.gdf.presentation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.xslf.usermodel.XMLSlideShow;

/**
 * Ez az osztáy előállít egy prezentációt.
 * 
 * @author Tamás Csizmadia
 * @author Gábor Nagy
 */
public class PresentationBuilder
{
    /**
     * A prezentáció, melyet a builder el fog készíteni.
     */
    private final XMLSlideShow slideShow;
    
    /**
     * A kész prezentáció fájlneve.
     */
    private String outputFilename;
    
    /**
     * Ez egy különleges dia, a cím-dia.
     */
    private TitleSlide titleSlide;
    
    /**
     * Példányosít egy prezentációt előllító builder osztályt.
     * 
     * @param masterpath A mester prezentáció fájlneve.
     * @throws IOException Ha a mester prezentáció nem elérhető.
     */
    public PresentationBuilder(String masterpath) throws IOException
    {
        slideShow = new XMLSlideShow(new FileInputStream(masterpath));
    }
    
    public PresentationBuilder() throws IOException
    {
        slideShow = new XMLSlideShow(
            this.getClass().getResourceAsStream("/EmptyPresentation.pptx")
        );
    }
    
    /**
     * Beállítja a végeredményként kiírandó prezentáció fájlnevét.
     * 
     * @param filename Ezen a néven menti ki a builder a kész prezentációt.
     */
    public void setOutputPresentation(String filename)
    {
        outputFilename = filename;
    }
    
    /**
     * Létrehozza az első, a prezentáció címét és alcímét tartalmazó diát.
     * 
     * @param title A prezentáció címe.
     * @param subtitle A prezentáció alcíme.
     */
    public void createTitleSlide(String title, String subtitle)
    {
        titleSlide = 
            (TitleSlide) SlideFactory.createSlide(SlideFactory.SlideType.TITLE);
        
        titleSlide.setTitle(title);
        titleSlide.setSubTitle(subtitle);
        titleSlide.render(slideShow);
    }
    
    /**
     * Hozzáad egy diát a prezentációhoz.
     * 
     * @param slide A dia, melyet hozzá fogunk adni a prezentációhoz.
     */
    public void addSlide(Slide slide)
    {
        slide.render(slideShow);
    }
    
    /**
     * Visszaadja a prezentáció cím-diáját.
     * 
     * @return A prezentáció cím-diája.
     */
    public TitleSlide getTitleSlide()
    {
        return titleSlide;
    }
    
    /**
     * Kiírja a létrehozott prezentációt.
     * 
     * @throws FileNotFoundException Ez a kivétel elvileg sosem váltható ki ezzel a metódussal.
     * @throws IOException IO hiba esetén.
     * @see setOutputPresentation
     */
    public void write() throws FileNotFoundException, IOException
    {
        FileOutputStream output = new FileOutputStream(outputFilename);
        slideShow.write(output);        
    }
    
    /**
     * Beállítja a diák méretét, képpontban.
     * 
     * @param width a diák szélessége.
     * @param height a diák magassága.
     */
    public void setPageSize(int width, int height)
    {
        slideShow.setPageSize(new java.awt.Dimension(width, height));
    }
}
