package edu.gdf.presentation;

/**
 * Ez, a Factory tervezési mintát követő osztály a createSlide statikus
 * metóduson keresztül előállít egy diát annak megfelelően, mit kell annak
 * tartalmaznia.
 * 
 * @author Tamás Csizmadia
 * @author Gábor Nagy
 * @see TitleSlide
 * @see ImageSlide
 * @see TextSlide
 */
public class SlideFactory
{
    /**
     * A különböző dia típusokat tartalmazó enum.
     */
    public static enum SlideType
    {
        /**
         * Címet és alcímet tartalmazó dia típus.
         */
        TITLE, 
        
        /**
         * Szöveget tartalmazó dia típus.
         */
        TEXT, 
        
        /**
         * Képet tartalmazó dia típus.
         */
        IMAGE
    }
    
    /**
     * Elkészít egy új diát a megadott típus szerint. A kész objektumok az
     * alapértelmezett üres konstruktorral kerülnek példányosításra.
     * <p>
     * Ezek után szükség lesz még a dia címének beállítására és más, az adott
     * diára jellemző műveletekre.
     * Mivel a metódus visszatérési típusa a Slide interface, szükség van a
     * megfelelő dia típusának cast-olására.
     * </p>
     * <pre>
     * {@code
     * TitleSlide elsoDia = 
     *  (TitleSlide) SlideFactory.createSlide(SlideFactory.SlideType.TITLE);
     * 
     * elsoDia.setTitle("Dia címe");
     * elsoDia.setSubTitle("A dia alcíme");
     * }
     * </pre>
     * @param slideType Az elkészítendő dia típusa.
     * @return Egy új dia.
     * @throws IllegalArgumentException Érvénytelen dia típus esetén.
     */
    public static Slide createSlide(SlideType slideType) 
        throws IllegalArgumentException
    {
        switch (slideType)
        {
            case TITLE:
                return new TitleSlide();
                
            case TEXT:
                return new TextSlide();
                
            case IMAGE:
                return new ImageSlide();
        }
        
        throw new IllegalArgumentException("Nem támogatott dia formátum");
    }
}
