package edu.gdf.presentation;

import java.awt.Graphics2D;
import org.apache.poi.xslf.usermodel.SlideLayout;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFSlideLayout;
import org.apache.poi.xslf.usermodel.XSLFSlideMaster;
import org.apache.poi.xslf.usermodel.XSLFTextShape;

/**
 * Ez a dia különleges szereppel bír - csak egy van belőle minden
 * prezentációban.
 * 
 * @author Tamás Csizmadia
 * @author Gábor Nagy
 */
public class TitleSlide implements Slide
{
    private String title;
    
    private String subTitle;
    
    /**
     * A render metódus által létrehozott Apache POI dia objektum.
     */
    private XSLFSlide slide;
    
    @Override
    public void render(XMLSlideShow slideshow)
    {
        XSLFSlideMaster master = slideshow.getSlideMasters()[0];
        
        XSLFSlideLayout layout = 
            master.getLayout(SlideLayout.TITLE);
        
        slide = slideshow.createSlide(layout);
        
        XSLFTextShape titleShape = slide.getPlaceholder(0);
        titleShape.setText(getTitle());
        
        XSLFTextShape subTitleShape = slide.getPlaceholder(1);
        subTitleShape.setText(getSubTitle());
    }

    @Override
    public String getTitle()
    {
        return title;
    }

    @Override
    public void setTitle(String title)
    {
        this.title = title;
    }

    @Override
    public XSLFSlide getSlide() throws RuntimeException
    {
        if (slide == null)
        {
            throw new RuntimeException(
                "A dia nem hozzáférhető az objektum jelen állapotában: valószínűleg nem hívta még meg a render metódust!"
            );
        }
        
        return this.slide;
    }

    /**
     * Visszaadja a prezentáció alcímét.
     * 
     * @return A prezentáció alcíme.
     */
    public String getSubTitle()
    {
        return subTitle;
    }

    /**
     * Beállítja a prezentáció alcímét.
     * 
     * @param subTitle A prezentáció alcíme.
     */
    public void setSubTitle(String subTitle)
    {
        this.subTitle = subTitle;
    }

    @Override
    public void draw(Graphics2D graphics)
    {
        slide.draw(graphics);
    }
    
}
