package edu.gdf.presentation;

import java.awt.Graphics2D;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;

/**
 * Egy diát megvalósító interface. A prezentációt a render metódusban
 * átadott paraméterrel injektáljuk a folyamatba.
 * 
 * @author Tamás Csizmadia
 * @author Gábor Nagy
 */
public interface Slide
{
    /**
     * Elkészíti a dia képét, amelyet megjeleníthetünk a bemutató során.
     * 
     * @param slideshow A prezentáció Apache POI objektuma.
     */
    public void render(XMLSlideShow slideshow);
    
    /**
     * Visszaadja a dia címét.
     * 
     * @return A dia címe.
     */
    public String getTitle();
    
    /**
     * Beállítja a dia címét.
     * 
     * @param title A dia címe.
     */
    public void setTitle(String title);
    
    /**
     * Visszaadja a kész diát tartalmazó Apache POI objektumot.
     * 
     * @return A diát tartalmazó Apache POI objektum.
     */
    public XSLFSlide getSlide();
    
    /**
     * A dia tartalmát leképezi egy Graphics2D objektumon.
     * 
     * @param graphics A dia tartalma itt fog megjelenni.
     */
    public void draw(Graphics2D graphics);
}
