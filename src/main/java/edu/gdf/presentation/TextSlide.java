package edu.gdf.presentation;

import java.awt.Graphics2D;
import java.util.ArrayList;
import org.apache.poi.xslf.usermodel.SlideLayout;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFSlideLayout;
import org.apache.poi.xslf.usermodel.XSLFSlideMaster;
import org.apache.poi.xslf.usermodel.XSLFTextShape;

/**
 * Egy szöveges diát reprezentáló osztály.
 * 
 * @author Tamás Csizmadia
 * @author Gábor Nagy
 */
public class TextSlide implements Slide
{
    /**
     * A dia fejléce.
     */
    private String title;
    
    /**
     * A render metódus által létrehozott Apache POI dia objektum.
     */
    private XSLFSlide slide;
    
    /**
     * A szöveges tartalom.
     */
    private final ArrayList<String> body;
    
    public TextSlide()
    {
        body = new ArrayList<>();
    }
    
    /**
     * Hozzáad egy bekezdést a dia szöveg szekciójához.
     * 
     * @param text A bekezdés szövege.
     */
    public void addParagraph(String text)
    {
        body.add(text);
    }
    
    @Override
    public void render(XMLSlideShow slideshow)
    {
        XSLFSlideMaster master = slideshow.getSlideMasters()[0];
        
        XSLFSlideLayout layout = 
            master.getLayout(SlideLayout.TITLE_AND_CONTENT);
        
        slide = slideshow.createSlide(layout);
        
        XSLFTextShape titleShape = slide.getPlaceholder(0);
        titleShape.setText(getTitle());
        
        XSLFTextShape bodyShape = slide.getPlaceholder(1);
        
        bodyShape.clearText();
        
        for (String currentParagraph : body) {
            bodyShape.addNewTextParagraph().addNewTextRun().setText(
                currentParagraph
            );
        }
    }

    /**
     * Visszaadja a dia fejlécét.
     * 
     * @return A dia fejléce.
     */
    @Override
    public String getTitle()
    {
        return title;
    }

    /**
     * Beállítja a dia fejlécét.
     * 
     * @param title A dia fejléce.
     */
    @Override
    public void setTitle(String title)
    {
        this.title = title;
    }
    
    /**
     * Visszaadja a létrehozott Apache POI dia objektumot.
     * 
     * @return A létrehozott dia Apache POI objektuma.
     * @throws RuntimeException Ha a dia még nem készült el.
     * @see render
     */
    @Override
    public XSLFSlide getSlide() throws RuntimeException
    {
        if (slide == null)
        {
            throw new RuntimeException(
                "A dia nem hozzáférhető az objektum jelen állapotában: valószínűleg nem hívta még meg a render metódust!"
            );
        }
        
        return this.slide;
    }

    @Override
    public void draw(Graphics2D graphics)
    {
        slide.draw(graphics);
    }
}
