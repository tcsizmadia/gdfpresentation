package edu.gdf.presentation.viewer;

import com.sun.media.jfxmedia.logging.Logger;
import edu.gdf.presentation.ImageSlide;
import edu.gdf.presentation.PresentationBuilder;
import edu.gdf.presentation.Slide;
import edu.gdf.presentation.SlideFactory;
import edu.gdf.presentation.TextSlide;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 * Prezentációt megjelenítő alkalmazás. Tartalmaz egy gombsort, valamint
 * egy panelt, amely a prezentáció egyes diáit jeleníti meg.
 * <p>
 * Az alkalmazás a PresentationBuilder osztály segítségével állítja
 * elő az egyes diákat. A diák draw() metódusával az aktuális dia
 * leképezhető az alkalmazás JPanel-ére.
 * </p>
 * <p>
 * A PresentationBuilder write() metódusával lemezre írjuk a legenerált
 * prezentációt a megfelelő gomb megnyomása után.
 * </p>
 * 
 * @author Tamás Csizmadia
 * @author Gábor Nagy
 */
public class Viewer extends javax.swing.JFrame
{
    /**
     * A prezentáció-készítő PresentationBuilder objektumunk.
     */
    private PresentationBuilder builder;
    
    /**
     * A diákat nyilvántartó osztály.
     */
    private final ArrayList<Slide> slides = new ArrayList<Slide>();
    
    /**
     * Az aktuális diát tartalmazó segédváltozó.
     */
    private int currentSlide = 0;
    
    /**
     * A panelPresentation JPanel-en megrajzolja az aktuális diát.
     */
    private void drawCurrentSlide()
    {
        if (slides.size() > 0) {
            slides.get(currentSlide).draw(
                (Graphics2D) panelPresentation.getGraphics()
            );            
        }
    }
    
    /**
     * Ez a segédmetódus elkészíti a prezentációt képező diákat és a
     * megjelenítő alkalmazás számára nyilvántartásba veszi a belső
     * ArrayList kollekcióban.
     */
    private void createSlides()
    {
        slides.clear();
        
        TextSlide firstSlide =
            (TextSlide) SlideFactory.createSlide(SlideFactory.SlideType.TEXT);

        firstSlide.setTitle("Bevezetés");

        firstSlide.addParagraph(
            "A projektünk egy prezentációt objektum-orientált módon előállító Java alkalmazás."
        );

        firstSlide.addParagraph(
            "Lényegében a szoftver állítja elő az önmagát bemutató prezentációt."
        );
        
        firstSlide.addParagraph("Az MVC paradigmát követve:");
        firstSlide.addParagraph("MODEL: Microsoft Powerpoint fájlformátuma");
        firstSlide.addParagraph("VIEW: Egy Java Swing alkalmazás");
        firstSlide.addParagraph("CONTROLLER: PresentationBuilder osztály");
        
        TextSlide secondSlide = 
            (TextSlide) SlideFactory.createSlide(SlideFactory.SlideType.TEXT);
        
        secondSlide.setTitle("A Program Funkciói");
        
        secondSlide.addParagraph(
            "A prezentáció elkészítése objektum-orientált módon, 'on-the-fly'"
        );
        
        secondSlide.addParagraph("A kész prezentáció bemutatása");
        secondSlide.addParagraph("A kész prezentáció mentése lemezre");
        secondSlide.addParagraph(
            "A lementett prezentáció megnyitható Microsoft Powerpoint alkalmazásból"
        );
        
        TextSlide thirdSlide = 
            (TextSlide) SlideFactory.createSlide(SlideFactory.SlideType.TEXT);
        
        thirdSlide.setTitle("Osztályok - Áttekintés");
        thirdSlide.addParagraph("Slide interface");
        thirdSlide.addParagraph("Slide implementációi");
        thirdSlide.addParagraph("SlideFactory osztály");
        thirdSlide.addParagraph("PresentationBuilder osztály");
        thirdSlide.addParagraph("Viewer Java Swing alkalmazás");
        
        ImageSlide umlSlideSlide = 
                (ImageSlide) SlideFactory.createSlide(
                    SlideFactory.SlideType.IMAGE
                );
        
        umlSlideSlide.setTitle("A Slide interface UML diagramja");
        umlSlideSlide.setImageFilename("/slide_uml.png");
        
        ImageSlide umlTextSlideSlide = 
                (ImageSlide) SlideFactory.createSlide(
                    SlideFactory.SlideType.IMAGE
                );
        
        umlTextSlideSlide.setTitle("A TextSlide osztály UML diagramja");
        umlTextSlideSlide.setImageFilename("/textslide_uml.png");
        
        ImageSlide umlImageSlideSlide = 
                (ImageSlide) SlideFactory.createSlide(
                    SlideFactory.SlideType.IMAGE
                );
        
        umlImageSlideSlide.setTitle("Az ImageSlide osztály UML diagramja");
        umlImageSlideSlide.setImageFilename("/imageslide_uml.png");
        
        ImageSlide umlTitleSlideSlide = 
                (ImageSlide) SlideFactory.createSlide(
                    SlideFactory.SlideType.IMAGE
                );
        
        umlTitleSlideSlide.setTitle("Az ImageSlide osztály UML diagramja");
        umlTitleSlideSlide.setImageFilename("/titleslide_uml.png");
        
        ImageSlide umlSlideFactorySlide = 
                (ImageSlide) SlideFactory.createSlide(
                    SlideFactory.SlideType.IMAGE
                );
        
        umlSlideFactorySlide.setTitle("A SlideFactory osztály UML diagramja");
        umlSlideFactorySlide.setImageFilename("/slidefactory_uml.png");
        
        ImageSlide umlPresentationBuilderSlide = 
                (ImageSlide) SlideFactory.createSlide(
                    SlideFactory.SlideType.IMAGE
                );
        
        umlPresentationBuilderSlide.setTitle("A PresentationBuilder osztály UML diagramja");
        umlPresentationBuilderSlide.setImageFilename("/presbuilder_uml.png");
        
        ImageSlide projectUmlSlide = 
                (ImageSlide) SlideFactory.createSlide(
                    SlideFactory.SlideType.IMAGE
                );
        
        projectUmlSlide.setTitle("Projekt UML diagramja");
        projectUmlSlide.setImageFilename("/project_uml.png");
        
        ImageSlide goodbyeSlide = 
                (ImageSlide) SlideFactory.createSlide(
                    SlideFactory.SlideType.IMAGE
                );
        
        goodbyeSlide.setTitle("Köszönjük a figyelmet!");
        goodbyeSlide.setImageFilename("/gdf_logo.png");
        
        TextSlide slideInterfaceSlide = 
            (TextSlide) SlideFactory.createSlide(SlideFactory.SlideType.TEXT);
        
        slideInterfaceSlide.setTitle("A Slide Interface");
        slideInterfaceSlide.addParagraph(
            "Ez az interface egy általános diát ír le a prezentációban."
        );
        
        slideInterfaceSlide.addParagraph(
            "Az egyes, specifikus diák ezt az interface-t implementálják."
        );
        
        slideInterfaceSlide.addParagraph(
            "getTitle() és setTitle() metódusok"
        );
        
        slideInterfaceSlide.addParagraph(
            "getSlide() metódus"
        );
        
        slideInterfaceSlide.addParagraph(
            "render() metódus"
        );
        
        slideInterfaceSlide.addParagraph(
            "draw() metódus"
        );
        
        TextSlide textSlideClassSlide = 
            (TextSlide) SlideFactory.createSlide(SlideFactory.SlideType.TEXT);

        textSlideClassSlide.setTitle("A TextSlide Osztály");
        textSlideClassSlide.addParagraph("Slide interface-t implementálja");
        textSlideClassSlide.addParagraph("Bekezdéseket tartalmaz (ez a dia is TextSlide)");
        textSlideClassSlide.addParagraph("body ArrayList kollekcióban tároljuk");
        textSlideClassSlide.addParagraph("addParagraph() metódus");
        
        TextSlide imageSlideClassSlide = 
            (TextSlide) SlideFactory.createSlide(SlideFactory.SlideType.TEXT);

        imageSlideClassSlide.setTitle("Az ImageSlide Osztály");
        imageSlideClassSlide.addParagraph("Slide interface-t implementálja");
        imageSlideClassSlide.addParagraph("Egy képet (PNG), és egy képaláírást tartalmaz");
        imageSlideClassSlide.addParagraph("setImageFilename() és getImageFilename() metódus");
        imageSlideClassSlide.addParagraph("setTitle() és getTitle() metódus (képaláírás)");
        
        TextSlide titleSlideClassSlide = 
            (TextSlide) SlideFactory.createSlide(SlideFactory.SlideType.TEXT);

        titleSlideClassSlide.setTitle("A TitleSlide Osztály");
        titleSlideClassSlide.addParagraph("Slide interface-t implementálja");
        titleSlideClassSlide.addParagraph("Rendhagyó dia, ebből csak egy létezhet egy prezentációban");
        titleSlideClassSlide.addParagraph("setTitle() és getTitle() metódus (cím)");
        titleSlideClassSlide.addParagraph("setSubTitle() és getSubTitle() metódus (alcím)");
        
        TextSlide slideFactoryClassSlide = 
            (TextSlide) SlideFactory.createSlide(SlideFactory.SlideType.TEXT);

        slideFactoryClassSlide.setTitle("A SlideFactory Osztály");
        slideFactoryClassSlide.addParagraph("Factory tervezési mintát követi");
        slideFactoryClassSlide.addParagraph("A hívónak nem kell törődnie a példányosítás menetével");
        slideFactoryClassSlide.addParagraph("Ez egy közös interface-el bíró objektumokat gyártó osztály");
        slideFactoryClassSlide.addParagraph("Ez az interface a Slide");
        slideFactoryClassSlide.addParagraph("Rugalmasan bővíthető a diák előállításának üzleti logikája");
        slideFactoryClassSlide.addParagraph("Jelenleg egyetlen statikus metódussal rendelkezik: createSlide");
        
        TextSlide presentationBuilderClassSlide = 
            (TextSlide) SlideFactory.createSlide(SlideFactory.SlideType.TEXT);

        presentationBuilderClassSlide.setTitle("A PresentationBuilder Osztály");
        presentationBuilderClassSlide.addParagraph("Builder tervezési mintát követi");
        presentationBuilderClassSlide.addParagraph("Egy komplex prezentáció elkészítését végzi");
        presentationBuilderClassSlide.addParagraph("'Gyári' sablon alapján készíti el a prezentációt");
        presentationBuilderClassSlide.addParagraph("Meghagyjuk a kontrolt a build műveletben a hívónak");
        presentationBuilderClassSlide.addParagraph("createTitleSlide() metódus");
        presentationBuilderClassSlide.addParagraph("setPageSize() metódus");
        presentationBuilderClassSlide.addParagraph("addSlide() metódus");
        presentationBuilderClassSlide.addParagraph("write() metódus");
        
        builder.createTitleSlide(
            "OOP Projekt: Prezentáció", "Nagy Gábor és Csizmadia Tamás"
        );
        
        slides.add(firstSlide);
        slides.add(secondSlide);
        slides.add(thirdSlide);
        slides.add(umlSlideSlide);
        slides.add(slideInterfaceSlide);
        slides.add(umlTextSlideSlide);
        slides.add(textSlideClassSlide);
        slides.add(umlImageSlideSlide);
        slides.add(imageSlideClassSlide);
        slides.add(umlTitleSlideSlide);
        slides.add(titleSlideClassSlide);
        slides.add(umlSlideFactorySlide);
        slides.add(slideFactoryClassSlide);
        slides.add(umlPresentationBuilderSlide);
        slides.add(presentationBuilderClassSlide);
        slides.add(projectUmlSlide);
        slides.add(goodbyeSlide);
        
        for (Slide currentSlide : slides) {
            builder.addSlide(currentSlide);
        }
        
        slides.add(0, builder.getTitleSlide());
    }
    
    /**
     * A következő diára lépteti a metódus a prezentáció megjelenítőt.
     * Ha az utolsó dián állt a program, az első diára ugrik vissza.
     */
    private void nextSlide()
    {
        if (currentSlide == (slides.size() - 1)) {
            currentSlide = 0;
        } else {
            currentSlide++;
        }
    }
    
    /**
     * Ezzel a metódussal az előző diára ugrik a prezentáció megjelenítő.
     * Ha az első dián állt a program, az utolsó diára ugrik.
     */
    private void prevSlide()
    {
        if (currentSlide > 0) {
            currentSlide--;
        } else {
            currentSlide = slides.size() - 1;
        }
    }
    
    /**
     * Példányosít egy prezentációt bemutató alkalmazást.
     */
    public Viewer()
    {
        initComponents();
        
        try
        {
            builder = new PresentationBuilder();
            builder.setPageSize(800, 400);
        }
        catch (Exception ex)
        {
            JOptionPane.showMessageDialog(
                rootPane, 
                String.format(
                    "Hiba lépett fel az alkalmazás indítása során: %s", 
                    ex.getMessage()
                )
            );
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jToolBar1 = new javax.swing.JToolBar();
        buttonExit = new javax.swing.JButton();
        buttonCreate = new javax.swing.JButton();
        buttonSave = new javax.swing.JButton();
        buttonPrev = new javax.swing.JButton();
        buttonNext = new javax.swing.JButton();
        panelPresentation = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(810, 670));
        setResizable(false);
        addComponentListener(new java.awt.event.ComponentAdapter()
        {
            public void componentResized(java.awt.event.ComponentEvent evt)
            {
                frameResized(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyPressed(java.awt.event.KeyEvent evt)
            {
                formKeyPressed(evt);
            }
        });

        jToolBar1.setFloatable(false);

        buttonExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/Door.png"))); // NOI18N
        buttonExit.setText("Kilépés");
        buttonExit.setToolTipText("Kilépés az alkalmazásból");
        buttonExit.setFocusable(false);
        buttonExit.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonExit.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonExit.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                buttonExitActionPerformed(evt);
            }
        });
        jToolBar1.add(buttonExit);

        buttonCreate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/Presentation.png"))); // NOI18N
        buttonCreate.setText("Generálás");
        buttonCreate.setToolTipText("Prezentáció elkészítése");
        buttonCreate.setFocusable(false);
        buttonCreate.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonCreate.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonCreate.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                buttonCreateActionPerformed(evt);
            }
        });
        jToolBar1.add(buttonCreate);

        buttonSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/Pen.png"))); // NOI18N
        buttonSave.setText("Mentés");
        buttonSave.setEnabled(false);
        buttonSave.setFocusable(false);
        buttonSave.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonSave.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonSave.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                buttonSaveActionPerformed(evt);
            }
        });
        jToolBar1.add(buttonSave);

        buttonPrev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/left-arrow-icone-4038-32.png"))); // NOI18N
        buttonPrev.setText("Előző Dia");
        buttonPrev.setToolTipText("Visszalépés az előző diára");
        buttonPrev.setEnabled(false);
        buttonPrev.setFocusable(false);
        buttonPrev.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonPrev.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonPrev.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                buttonPrevActionPerformed(evt);
            }
        });
        jToolBar1.add(buttonPrev);

        buttonNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/right-icone-8882-32.png"))); // NOI18N
        buttonNext.setText("Következő Dia");
        buttonNext.setToolTipText("Ugrás a következő diára");
        buttonNext.setEnabled(false);
        buttonNext.setFocusable(false);
        buttonNext.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonNext.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonNext.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                buttonNextActionPerformed(evt);
            }
        });
        jToolBar1.add(buttonNext);

        panelPresentation.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelPresentation.addComponentListener(new java.awt.event.ComponentAdapter()
        {
            public void componentResized(java.awt.event.ComponentEvent evt)
            {
                panelResized(evt);
            }
        });

        javax.swing.GroupLayout panelPresentationLayout = new javax.swing.GroupLayout(panelPresentation);
        panelPresentation.setLayout(panelPresentationLayout);
        panelPresentationLayout.setHorizontalGroup(
            panelPresentationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        panelPresentationLayout.setVerticalGroup(
            panelPresentationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 596, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 795, Short.MAX_VALUE)
                    .addComponent(panelPresentation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelPresentation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonExitActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_buttonExitActionPerformed
    {//GEN-HEADEREND:event_buttonExitActionPerformed
        dispose();
    }//GEN-LAST:event_buttonExitActionPerformed

    private void buttonCreateActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_buttonCreateActionPerformed
    {//GEN-HEADEREND:event_buttonCreateActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            try {
                
                createSlides();
                drawCurrentSlide();
                
                buttonNext.setEnabled(true);
                buttonPrev.setEnabled(true);
                buttonSave.setEnabled(true);
            } catch (Exception ex) {
                Logger.logMsg(
                        Logger.ERROR, 
                        String.format("Hiba lépett fel: %s", ex.getMessage())
                );
            }
        });

    }//GEN-LAST:event_buttonCreateActionPerformed

    private void frameResized(java.awt.event.ComponentEvent evt)//GEN-FIRST:event_frameResized
    {//GEN-HEADEREND:event_frameResized
        try {
            drawCurrentSlide();
        } catch (Exception ex) {
            Logger.logMsg(
                Logger.ERROR, 
                String.format("Hiba lépett fel: %s", ex.getMessage())
            );
        }
    }//GEN-LAST:event_frameResized

    private void panelResized(java.awt.event.ComponentEvent evt)//GEN-FIRST:event_panelResized
    {//GEN-HEADEREND:event_panelResized
        builder.setPageSize(
            panelPresentation.getWidth(), 
            panelPresentation.getHeight()
        );
        
        drawCurrentSlide();
    }//GEN-LAST:event_panelResized

    private void buttonSaveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_buttonSaveActionPerformed
    {//GEN-HEADEREND:event_buttonSaveActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            builder.setOutputPresentation(
                System.getProperty("user.dir").concat(File.separator).concat("Presentation.pptx")
            );

            try {
                builder.write();
                
                JOptionPane.showMessageDialog(
                    rootPane, 
                    "A prezentációt Presentation.pptx néven elmentettük!"
                );
                
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(
                    rootPane, 
                    String.format(
                        "Hiba lépett fel a mentés során: %s", 
                        ex.getMessage()
                    )
                );
            }
        });

    }//GEN-LAST:event_buttonSaveActionPerformed

    private void buttonPrevActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_buttonPrevActionPerformed
    {//GEN-HEADEREND:event_buttonPrevActionPerformed
        prevSlide();
        drawCurrentSlide();
    }//GEN-LAST:event_buttonPrevActionPerformed

    private void buttonNextActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_buttonNextActionPerformed
    {//GEN-HEADEREND:event_buttonNextActionPerformed
        nextSlide();
        drawCurrentSlide();
    }//GEN-LAST:event_buttonNextActionPerformed

    private void formKeyPressed(java.awt.event.KeyEvent evt)//GEN-FIRST:event_formKeyPressed
    {//GEN-HEADEREND:event_formKeyPressed
        switch (evt.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
                nextSlide();
                drawCurrentSlide();
                break;
                
            case KeyEvent.VK_LEFT:
                prevSlide();
                drawCurrentSlide();
                break;
        }
    }//GEN-LAST:event_formKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Viewer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Viewer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Viewer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Viewer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new Viewer().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonCreate;
    private javax.swing.JButton buttonExit;
    private javax.swing.JButton buttonNext;
    private javax.swing.JButton buttonPrev;
    private javax.swing.JButton buttonSave;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JPanel panelPresentation;
    // End of variables declaration//GEN-END:variables
}
