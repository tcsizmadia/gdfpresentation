package edu.gdf.presentation;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xslf.usermodel.SlideLayout;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFPictureData;
import org.apache.poi.xslf.usermodel.XSLFPictureShape;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFSlideLayout;
import org.apache.poi.xslf.usermodel.XSLFSlideMaster;
import org.apache.poi.xslf.usermodel.XSLFTextShape;

/**
 * Egy képet tartalmazó diát reprezentáló osztály. Ennél a diánál a cím
 * képaláírásként funkcionál.
 * 
 * @author Tamás Csizmadia
 * @author Gábor Nagy
 */
public class ImageSlide implements Slide
{
    private String title;

    /**
     * A render metódus által létrehozott Apache POI dia objektum.
     */
    private XSLFSlide slide;
    
    /**
     * A dián szereplő kép elérési útja.
     */
    private String imageFilename;
    
    @Override
    public void render(XMLSlideShow slideshow)
    {
        XSLFSlideMaster master = slideshow.getSlideMasters()[0];
        
        XSLFSlideLayout layout = 
            master.getLayout(SlideLayout.PIC_TX);
        
        slide = slideshow.createSlide(layout);

        XSLFTextShape titleShape = slide.getPlaceholder(0);
        titleShape.setText(getTitle());
        
        XSLFTextShape subTitleShape = slide.getPlaceholder(2);
        subTitleShape.setText("");
        
        byte[] pictureData;
        
        try {
            pictureData = 
                IOUtils.toByteArray(
                    getClass().getResourceAsStream(getImageFilename())
                );
        } catch (FileNotFoundException ex) {
            throw new RuntimeException("A kép nem található");
        } catch (IOException ex) {
            throw new RuntimeException(
                String.format(
                    "Hiba lépett fel a kép ('%s') feldolgozásakor", 
                    getImageFilename()
                )
            );
        }
        
        int picIndex = 
            slideshow.addPicture(pictureData, XSLFPictureData.PICTURE_TYPE_PNG);
        
        XSLFPictureShape picture = slide.createPicture(picIndex);
        
        XSLFTextShape placeholder = slide.getPlaceholder(1);
        Rectangle2D anchor = placeholder.getAnchor();
        
        slide.removeShape(placeholder);
        picture.setAnchor(anchor);
    }

    @Override
    public String getTitle()
    {
        return title;
    }

    @Override
    public void setTitle(String title)
    {
        this.title = title;
    }

    @Override
    public XSLFSlide getSlide() throws RuntimeException
    {
        if (slide == null)
        {
            throw new RuntimeException(
                "A dia nem hozzáférhető az objektum jelen állapotában: valószínűleg nem hívta még meg a render metódust!"
            );
        }
        
        return this.slide;
    }

    /**
     * Visszaadja a dián szereplő kép fájlnevét.
     * 
     * @return A dián szereplő kép fájlneve.
     */
    public String getImageFilename()
    {
        return imageFilename;
    }

    /**
     * Beállítja a dián szereplő kép fájlnevét.
     * 
     * @param imageFilename A dián szereplő kép fájlneve.
     */
    public void setImageFilename(String imageFilename)
    {
        this.imageFilename = imageFilename;
    }

    @Override
    public void draw(Graphics2D graphics)
    {
        slide.draw(graphics);
    }
}
