# PRESENTATION PROJEKT #

A Gábor Dénes Főiskola Objektumorientált Fejlesztés vizsgáján előadandó projektünk.

![Screen Shot 2015-01-19 at 12.25.50.png](https://bitbucket.org/repo/keaGGe/images/2772476109-Screen%20Shot%202015-01-19%20at%2012.25.50.png)

## A FELADAT ##

Egy komplett objektumorientált rendszer megtervezése, lefejlesztése, majd az elkészített munkáról egy rövid prezentáció megtartása a vizsgán.

## RENDSZERKÖVETELMÉNYEK ##

* Java 8 SE Runtime (fejlesztéshez JDK 8)
* Grafikus felhasználói felülettel rendelkező operációs rendszer (Windows, Linux, Mac OSX)
* Körülbelül 3 megabyte háttértár (nem számítva a generált prezentációt)
* Egér és/vagy billentyűzet a prezentáció kezeléséhez

## TELEPÍTÉS ##

Az aktuális verzió a [Letöltések](https://bitbucket.org/tcsizmadia/gdfpresentation/downloads) szekcióban található, **Presentation.zip** tömörített állományként. Kicsomagolás után a következő paranccsal indítható az alkalmazás:


```
#!shell

java -jar Presentation-1.0-SNAPSHOT.jar
```


## KÉSZÍTŐK ##

* Nagy Gábor
* Csizmadia Tamás